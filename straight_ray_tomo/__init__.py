"""
:copyright:
    Christian Boehm (christian.boehm@erdw.ethz.ch)
    Naiara Korta Martiartu (naiara.korta@erdw.ethz.ch)
    Patrick Marty (patrick.marty@erdw.ethz.ch)
    ETH Zurich, 2018-21
:license:
    BSD 3-Clause ("BSD New" or "BSD Simplified")
"""

from .straight_ray_tracer import *  # NOQA
