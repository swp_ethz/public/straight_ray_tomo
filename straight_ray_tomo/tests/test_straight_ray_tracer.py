"""Test straight ray tracer.

:copyright:
    Christian Boehm (christian.boehm@erdw.ethz.ch)
    Naiara Korta Martiartu (naiara.korta@erdw.ethz.ch)
    Patrick Marty (patrick.marty@erdw.ethz.ch)
    ETH Zurich, 2018-21
:license:
    BSD 3-Clause ("BSD New" or "BSD Simplified")
"""

import straight_ray_tomo as srt

import numpy as np
import pytest
import xarray as xr
from unittest.mock import patch


def test_xarray_validation():

    ds = xr.Dataset(
        coords={
            "a": [0.0, 1.0],
        },
    )
    with pytest.raises(ValueError) as e:
        srt.validate_xarray_dataset(ds)
    assert e.value.args[0].startswith("Invalid dimensions of xarray data.")

    ds = xr.Dataset(
        coords={
            "x": [0.0, 1.0],
            "y": [0.0],
            "z": [0.0],
        },
    )
    with pytest.raises(ValueError) as e:
        srt.validate_xarray_dataset(ds)
    assert e.value.args[0].startswith("Invalid dimensions of xarray data.")

    ds = xr.Dataset(
        coords={
            "x": [0.0, 1.0],
            "y": [0.0],
        },
    )
    with pytest.raises(ValueError) as e:
        srt.validate_xarray_dataset(ds)
    assert e.value.args[0].startswith(
        "The grid must contain at least two points in each dimension."
    )

    ds = xr.Dataset(
        coords={
            "x": [0.0, 1.0],
            "y": [0.0, 1.0, 3.0],
        },
    )
    with pytest.raises(ValueError) as e:
        srt.validate_xarray_dataset(ds)
    assert e.value.args[0].startswith(
        "The grid spacing must be equidistant in each dimension."
    )

    ds = xr.Dataset(
        coords={
            "x": [0.0, -2.0],
            "y": [0.0, 2.0, 4.0],
        },
    )
    with pytest.raises(ValueError) as e:
        srt.validate_xarray_dataset(ds)
    assert e.value.args[0].startswith(
        "The grid values must be strictly increasing."
    )

    ds = xr.Dataset(
        coords={
            "x": [0.0, 3.0],
            "y": [1.0, 3.0, 5.0],
        },
    )
    origin, spacing, n_points = srt.validate_xarray_dataset(ds)
    np.testing.assert_allclose(origin, np.array([0.0, 1.0]))
    np.testing.assert_allclose(spacing, np.array([3.0, 2.0]))
    np.testing.assert_allclose(n_points, np.array([2, 3]))

    ds = xr.Dataset(
        coords={
            "x": [-1.0, 3.0],
            "y": [1.0, 1.5, 2.0],
        },
    )
    origin, spacing, n_points = srt.validate_xarray_dataset(ds)
    np.testing.assert_allclose(origin, np.array([-1.0, 1.0]))
    np.testing.assert_allclose(spacing, np.array([4.0, 0.5]))
    np.testing.assert_allclose(n_points, np.array([2, 3]))


def test_ray():

    origin = np.array([0.0, 0.0])
    spacing = np.array([1.0, 1.0])
    n_points = np.array([4, 3])

    for s, r, vals_ref, idx_ref in zip(
        [
            np.array([0.25, 0.1]),
            np.array([0.75, 0.1]),
            np.array([0.5, 0.1]),
            np.array([2.25, 1.1]),
            np.array([0.25, 1.1]),
            np.array([0.0, 0.0]),
            np.array([0.0, 0.0]),
            np.array([3.12, 0.4]),
        ],
        [
            np.array([0.25, 0.2]),
            np.array([0.75, 0.2]),
            np.array([0.5, 0.2]),
            np.array([0.25, 1.1]),
            np.array([2.25, 1.1]),
            np.array([1.0, 2.0]),
            np.array([1.0, 1.0]),
            np.array([-0.1, 2.1]),
        ],
        [
            [0.1],
            [0.1],
            [0.1],
            [0.25, 1.0, 0.75],
            [0.25, 1.0, 0.75],
            [
                0.25 * np.sqrt(5),
                0.25 * np.sqrt(5),
                0.25 * np.sqrt(5),
                0.25 * np.sqrt(5),
            ],
            None,
            None,
        ],
        [
            [0],
            [3],
            None,
            [1, 4, 7],
            [1, 4, 7],
            [0, 1, 4, 5],
            None,
            [2, 5, 4, 7, 10, 9],
        ],
    ):
        idx, vals = srt.compute_ray_path(
            source=s,
            receiver=r,
            origin=origin,
            spacing=spacing,
            n_points=n_points,
        )

        if vals_ref:
            np.testing.assert_allclose(vals, vals_ref)
        if idx_ref:
            np.testing.assert_allclose(idx, idx_ref)

        np.testing.assert_allclose(np.sum(vals), np.linalg.norm(s - r))


def test_forward_operator():

    sources = np.random.randn(10, 2)
    receivers = np.random.randn(10, 2)

    min_x = min(min(sources[:, 0]), min(receivers[:, 0]))
    max_x = max(max(sources[:, 0]), max(receivers[:, 0]))
    min_y = min(min(sources[:, 1]), min(receivers[:, 1]))
    max_y = max(max(sources[:, 1]), max(receivers[:, 1]))

    grid = xr.Dataset(
        coords={
            "x": np.linspace(min_x, max_x, 17, endpoint=True),
            "y": np.linspace(min_y, max_y, 8, endpoint=True),
        },
    )

    fwd = srt.create_forward_operator(sources, receivers, grid)
    np.testing.assert_allclose(
        fwd.sum(axis=1),
        np.linalg.norm(sources - receivers, axis=1)[:, np.newaxis],
    )


def test_get_all_to_all_locations():

    src = np.array([[1, 2], [3, 4]])
    rec = np.array([[1, 2], [0, 4], [0.5, 1.5]])

    srcs, recs = srt.get_all_to_all_locations(src, rec)

    np.testing.assert_allclose(recs[:3, :], rec)
    np.testing.assert_allclose(recs[3:, :], rec)
    np.testing.assert_allclose(srcs[:3, 0], src[0, 0])
    np.testing.assert_allclose(srcs[:3, 1], src[0, 1])
    np.testing.assert_allclose(srcs[3:, 0], src[1, 0])
    np.testing.assert_allclose(srcs[3:, 1], src[1, 1])


@patch("matplotlib.pyplot.show")
def test_plot_rays(mock_show):

    src = np.array([[1, 2], [3, 4]])
    rec = np.array([[1, 2], [0, 4], [0.5, 1.5]])

    srcs, recs = srt.get_all_to_all_locations(src, rec)

    fig, ax = srt.plot_rays(srcs, recs, only_locations=True)
    assert len(ax.lines) == 0
    assert ax.title.get_text() == "ray coverage"
    fig, ax = srt.plot_rays(srcs, recs, only_locations=False)
    assert len(ax.lines) == 6
    assert ax.title.get_text() == "ray coverage"


def test_plot_ray_density():

    src = np.array([[1, 2], [3, 4]])
    rec = np.array([[1, 2], [0, 4], [0.5, 1.5]])

    sources, receivers = srt.get_all_to_all_locations(src, rec)

    min_x = min(min(src[:, 0]), min(rec[:, 0]))
    max_x = max(max(src[:, 0]), max(rec[:, 0]))
    min_y = min(min(src[:, 1]), min(rec[:, 1]))
    max_y = max(max(src[:, 1]), max(rec[:, 1]))

    grid = xr.Dataset(
        coords={
            "x": np.linspace(min_x, max_x, 17, endpoint=True),
            "y": np.linspace(min_y, max_y, 8, endpoint=True),
        },
    )

    fwd = srt.create_forward_operator(sources, receivers, grid)
    fig, ax = srt.plot_ray_density(fwd, grid)
    # Not great, but at least ensure that something comes back
    assert ax.get_xlabel() == "x [m]"
    assert ax.get_ylabel() == "y [m]"
