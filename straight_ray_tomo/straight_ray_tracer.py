"""Straight ray tracer for regular grids in 2D and 3D.

:copyright:
    Christian Boehm (christian.boehm@erdw.ethz.ch)
    Naiara Korta Martiartu (naiara.korta@erdw.ethz.ch)
    Patrick Marty (patrick.marty@erdw.ethz.ch)
    ETH Zurich, 2018-21
:license:
    BSD 3-Clause ("BSD New" or "BSD Simplified")
"""

import matplotlib.pyplot as plt  # type: ignore
import numpy as np  # type: ignore
import scipy.sparse as sp  # type: ignore
import typing
import xarray as xr  # type: ignore


def validate_xarray_dataset(
    grid: xr.Dataset,
) -> typing.Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    Validate the grid.

    While xarray supports multi-dimensional and not equidistantly spaced
    rectilinear grids, we need to be more restrictive here.

    Parameters
    ----------
    grid : xr.Dataset
        xarray dataset containing the computational grid.

    Returns
    -------
    tuple
        3-component tuple containing the origin, grid size and number of points
        in each dimension.

    Raises
    ------
    ValueError
        If the xarray dataset has incorrect dimensions, dimension lables or
        is not equidistantly spaced.
    """

    def raise_dim_error():
        raise ValueError(
            "Invalid dimensions of xarray data. "
            "Dimension labels must be {`x`, `y`}."
        )

    if len(grid.coords.dims) not in [2, 3]:
        raise_dim_error()

    # Currently we only support 2D
    if not set(grid.coords.dims) == set(["x", "y"]):
        raise_dim_error()

    # Make sure there is at least one pixel in each dimension
    if len(grid.coords["x"]) < 2 or len(grid.coords["y"]) < 2:
        raise ValueError(
            "The grid must contain at least two points in each dimension. "
            f"Found {grid.coords}"
        )

    origin = np.array([grid.coords["x"].values[0], grid.coords["y"].values[0]])
    spacing = np.array(
        [
            grid.coords["x"].values[1] - grid.coords["x"].values[0],
            grid.coords["y"].values[1] - grid.coords["y"].values[0],
        ]
    )
    n_points = np.array([len(grid.coords["x"]), len(grid.coords["y"])])

    # Make sure the grid is equidistantly spaced
    dx = grid.coords["x"].values[1:] - grid.coords["x"].values[:-1]
    dy = grid.coords["y"].values[1:] - grid.coords["y"].values[:-1]
    if not np.allclose(dx, spacing[0]) or not np.allclose(dy, spacing[1]):
        raise ValueError(
            "The grid spacing must be equidistant in each dimension. "
            f"Found {grid.coords}"
        )

    if any(spacing <= 0):
        raise ValueError(
            "The grid values must be strictly increasing. "
            f"Found {grid.coords}"
        )

    return origin, spacing, n_points


def compute_ray_path(
    source: np.ndarray,
    receiver: np.ndarray,
    origin: np.ndarray,
    spacing: np.ndarray,
    n_points: np.ndarray,
) -> typing.Tuple[np.ndarray, np.ndarray]:
    """
    Compute the straight ray path between a source and a receiver.

    Returns the indices of all intersected pixels as well as the lengths of the
    ray within those pixels.

    Parameters
    ----------
    grid : xr.Dataset
        xarray dataset containing the computational grid.

    Returns
    -------
    tuple
        3-component tuple containing the origin, grid size and number of points
        in each dimension.

    Raises
    ------
    ValueError
        If the xarray dataset has incorrect dimensions, dimension lables or
        is not equidistantly spaced.
    """
    assert source.shape == (2,)
    assert receiver.shape == (2,)
    assert len(origin) == 2
    assert len(spacing) == 2
    assert len(n_points) == 2

    b_origin = origin - 0.5 * spacing

    delta = receiver - source
    if delta[0] > 0:
        six = int(np.floor((source[0] - b_origin[0]) / spacing[0]) + 1)
        rix = int(np.ceil((receiver[0] - b_origin[0]) / spacing[0]) - 1)
        nx = max(0, rix - six + 1)
    else:
        six = int(np.ceil((source[0] - b_origin[0]) / spacing[0]) - 1)
        rix = int(np.floor((receiver[0] - b_origin[0]) / spacing[0]) + 1)
        nx = max(0, six - rix + 1)

    if delta[1] > 0:
        siy = int(np.floor((source[1] - b_origin[1]) / spacing[1]) + 1)
        riy = int(np.ceil((receiver[1] - b_origin[1]) / spacing[1]) - 1)
        ny = max(0, riy - siy + 1)
    else:
        siy = int(np.ceil((source[1] - b_origin[1]) / spacing[1]) - 1)
        riy = int(np.floor((receiver[1] - b_origin[1]) / spacing[1]) + 1)
        ny = max(0, siy - riy + 1)

    numpoints = nx + ny + 2

    unsortedpoints = np.zeros((numpoints, 2))
    unsortedpoints[0, :] = source

    if nx == 1:
        unsortedpoints[1, 0] = b_origin[0] + six * spacing[0]
        unsortedpoints[1, 1] = (
            source[1]
            + (unsortedpoints[1, 0] - source[0]) / delta[0] * delta[1]
        )

    elif nx > 1:
        order = int(np.sign(rix - six))
        unsortedpoints[1 : nx + 1, 0] = (
            b_origin[0] + np.arange(six, six + order * nx, order) * spacing[0]
        )
        unsortedpoints[1 : nx + 1, 1] = (
            source[1]
            + (unsortedpoints[1 : nx + 1, 0] - source[0]) / delta[0] * delta[1]
        )

    if ny == 1:
        unsortedpoints[1 + nx, 1] = b_origin[1] + siy * spacing[1]
        unsortedpoints[1 + nx, 0] = (
            source[0]
            + (unsortedpoints[1 + nx, 1] - source[1]) / delta[1] * delta[0]
        )

    elif ny > 1:
        order = np.sign(riy - siy)
        unsortedpoints[1 + nx : 1 + nx + ny, 1] = (
            b_origin[1] + np.arange(siy, siy + order * ny, order) * spacing[1]
        )
        unsortedpoints[1 + nx : 1 + nx + ny, 0] = (
            source[0]
            + (unsortedpoints[1 + nx : 1 + nx + ny, 1] - source[1])
            / delta[1]
            * delta[0]
        )

    unsortedpoints[numpoints - 1, :] = receiver
    if unsortedpoints[:, 1].ptp() < 1e-6*spacing[1]:
        ix = np.argsort(unsortedpoints[:, 0])
    elif unsortedpoints[:, 0].ptp() < 1e-6*spacing[0]:
        ix = np.argsort(unsortedpoints[:, 1])
    else:
        ix = np.lexsort((unsortedpoints[:, 1], unsortedpoints[:, 0]))
    sortedpoints = unsortedpoints[ix]
    midpoints = (sortedpoints[0:-1, :] + sortedpoints[1:, :]) / 2

    ix = np.floor((midpoints[:, 0] - b_origin[0]) / spacing[0])
    iy = np.floor((midpoints[:, 1] - b_origin[1]) / spacing[1])
    # This should not happen.
    assert all(ix >= 0), f"Invalid pixel index: ({ix[ix<0]}, {iy[ix<0]})."
    assert all(iy >= 0), f"Invalid pixel index: ({ix[iy<0]}, {iy[iy<0]})."
    assert all(ix <= n_points[0] - 1), (
        "Invalid pixel index: "
        f"({ix[ix>n_points[0]-1]}, {iy[ix>n_points[0]-1]})."
    )
    assert all(iy <= n_points[1] - 1), (
        "Invalid pixel index: "
        f"({ix[iy>n_points[1]-1]}, {iy[iy>n_points[1]-1]})."
    )

    indices = n_points[1] * ix + iy
    vals = np.linalg.norm(sortedpoints[1:, :] - sortedpoints[0:-1, :], axis=1)

    return indices, vals


def create_forward_operator(
    sources: np.ndarray, receivers: np.ndarray, grid: xr.Dataset
) -> sp.csr_matrix:
    """Create forward operator matrix for straight ray tomography.

    Compute a sparse matrix reprenting the rays for each source-receiver pair.
    The dimensions of the matrix are (number of rays) x (number of pixels)
    and each non-zero entry encodes the length of the intersection of the ray
    with this pixel.

    Source and receiver arrays must have the same length.

    Parameters
    ----------
    sources: np.ndarray
        2-dimensional array (N x 2) of all source locations.
    receivers: np.ndarray
        2-dimensional array (N x 2) of all receiver locations.
    grid : xr.Dataset
        xarray dataset containing the computational grid.

    Returns
    -------
    scipy.sparse.csr_matrix
        Sparse matrix encoding the ray paths.

    """
    origin, spacing, n_points = validate_xarray_dataset(grid)

    # Determine size of the sparse matrix
    # The number of columns is equal to the number of pixels in the 2D grid
    # The number of rows is equal to the number of ray paths.
    ncol = n_points.prod()
    nrow = sources.shape[0]

    assert len(sources.shape) == 2
    assert len(receivers.shape) == 2
    assert sources.shape[1] == 2
    assert receivers.shape[1] == 2
    assert sources.shape[0] == receivers.shape[0]

    # There are several ways to create a sparse matrix in python.
    # Here, we use three vectors that contain the following information
    # - all_data: the actual values of the nonzero entries
    # - all_indices: the column indices of the nonzero entries
    # - all_indptr: the start idx in the above two vectors for each row

    # Pre-allocate space for the nonzero entries of the matrix.
    # This improves the efficiency when concatenating the indices and values
    # in memory. Here, we assume that any ray will intersect at most 2 times
    # the average number of pixels in x- and y-direction.
    all_indices = np.zeros((nrow * 2 * int(n_points.mean()),), dtype=int)
    all_data = np.zeros((nrow * 2 * int(n_points.mean()),))
    all_indptr = np.zeros((nrow + 1,), dtype=int)

    # Now loop through all source-receiver pairs and create the ray paths
    for i in range(0, nrow):
        [indices, vals] = compute_ray_path(
            sources[i], receivers[i], origin, spacing, n_points
        )
        all_indices[all_indptr[i] : all_indptr[i] + len(indices)] = indices
        all_data[all_indptr[i] : all_indptr[i] + len(indices)] = vals
        all_indptr[i + 1] = all_indptr[i] + len(indices)

    # sanity checks
    assert max(all_indices) < ncol
    assert min(all_indices) >= 0

    # After gathering data for all rays, we can now initialize the
    # sparse matrix.
    return sp.csr_matrix(
        (
            all_data[: all_indptr[nrow]],
            all_indices[: all_indptr[nrow]],
            all_indptr,
        ),
        shape=(nrow, ncol),
    )


def get_all_to_all_locations(
    source_locations: np.ndarray, receiver_locations: np.ndarray
) -> typing.Tuple[np.ndarray, np.ndarray]:
    """Obtain all-to-all source-receiver combinations.

    The inputs contain unique source and receiver positions.
    The output duplicates the locations to contain all individual ray paths.

    Parameters
    ----------
    source_locations: np.ndarray
        2-dimensional array (N x 2) of unique source locations.
    receiver_locations: np.ndarray
        2-dimensional array (N x 2) of unique receiver locations.

    Returns
    -------
    tuple
        2-component tuple, containing source and receiver locations.

    """
    nsrc = source_locations.shape[0]
    nrec = receiver_locations.shape[0]
    ndata = nsrc * nrec
    sources = np.zeros((ndata, 2))
    receivers = np.zeros((ndata, 2))
    for i in range(0, nsrc):
        sources[i * nrec : (i + 1) * nrec, :] = np.ones(
            (nrec, 2)
        ) * source_locations[i, :].reshape((1, 2))
        receivers[i * nrec : (i + 1) * nrec, :] = receiver_locations

    return sources, receivers


def plot_ray_density(A: sp.csr_matrix, grid: xr.Dataset):
    """Plot ray density of an acquisition.

    Parameters
    ----------
    A: scipy.sparse.csr_matrix
        Matrix containing all ray paths
    grid : xr.Dataset
        xarray dataset containing the computational grid.

    """
    ray_density = np.sum(A, axis=0)
    grid2 = grid.assign(
        ray_density=(
            ("x", "y"),
            ray_density.reshape(len(grid.coords["x"]), len(grid.coords["y"])),
        )
    )
    fig, ax = plt.subplots(figsize=(6, 6))
    grid2.ray_density.T.plot(axes=ax)
    ax.set_aspect("equal")
    plt.xlabel("x [m]")
    plt.ylabel("y [m]")
    plt.draw()

    return fig, ax


def plot_rays(
    sources: np.ndarray, receivers: np.ndarray, only_locations=False
):
    """Plot ray density of an acquisition.

    Parameters
    ----------
    source_locations: np.ndarray
        2-dimensional array (N x 2) of unique source locations.
    receiver_locations: np.ndarray
        2-dimensional array (N x 2) of unique receiver locations.
    only_locations: bool
        Optionally only plot source/receiver locations, but no ray paths
        between them.

    """
    assert sources.shape == receivers.shape
    assert sources.shape[1] == 2

    fig = plt.figure(figsize=(6, 6))
    ax = fig.add_subplot(111)
    if not only_locations:
        for i in range(sources.shape[0]):
            plt.plot(
                np.array([sources[i, 0], receivers[i, 0]]),
                np.array([sources[i, 1], receivers[i, 1]]),
                "k",
            )

    plt.scatter(sources[:, 0], sources[:, 1], marker="*")
    plt.scatter(receivers[:, 0], receivers[:, 1], marker="v", color="g")

    ax.set_aspect("equal")
    plt.xlabel("x [m]")
    plt.ylabel("y [m]")
    plt.title("ray coverage")
    plt.show()

    return fig, ax
