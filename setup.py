"""
Straight-ray tomography

Python tools for time-of-flight tomography in 2D / 3D using straight-ray
tracing.
"""
import inspect
import os
import sys

from setuptools import setup, find_packages


# Import the version string.
path = os.path.join(
    os.path.abspath(os.path.dirname(inspect.getfile(inspect.currentframe()))),
    "straight_ray_tomo",
)
sys.path.insert(0, path)


def get_package_data():
    """
    Returns a list of all files needed for the installation relative to the
    root folder.
    """
    filenames = []
    root_dir = os.path.join(
        os.path.dirname(
            os.path.abspath(inspect.getfile(inspect.currentframe()))
        ),
        "straight_ray_tomo",
    )
    # Recursively include all files in these folders:
    folders = [os.path.join(root_dir, "tests", "data")]
    for folder in folders:
        for directory, _, files in os.walk(folder):
            for filename in files:
                # Exclude hidden files.
                if filename.startswith("."):
                    continue
                filenames.append(
                    os.path.relpath(
                        os.path.join(directory, filename), root_dir
                    )
                )
    return filenames


setup_config = dict(
    name="straight_ray_tomo",
    version="0.1",
    description="Straight-ray tomography",
    author="Christian Boehm",
    author_email="christian.boehm@erdw.ethz.ch",
    packages=find_packages(),
    license="MIT/BSD",
    platforms="OS Independent",
    install_requires=[
        "h5py",
        "lxml",
        "matplotlib",
        "numpy",
        "scipy",
    ],
    package_data={"straight_ray_tomo": get_package_data()},
)


if __name__ == "__main__":
    setup(**setup_config)
